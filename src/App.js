import React from "react";

import Navbar from "./Components/Navbar";
import Carousel from "./Components/Carousel";
import Cards from "./Components/Cards";
import Pagination from "./Components/Pagination";
import Footer from "./Components/Footer";

function App() {
  return (
    <div>
      < Navbar />
      < Carousel />
      < Cards />
      < Pagination />
      < Footer />
    </div>
  );
}

export default App;
