import React, { Component } from 'react'
import CardUI from './CardUI';

class Cards extends Component {
    render() {
        return (
            <div>
                <div className="d-flex justify-content-around">
                    <div className="row">
                        <div className="col-md-4">
                            <CardUI />
                        </div>
                        <div className="col-md-4">
                            <CardUI />
                        </div>
                        <div className="col-md-4">
                            <CardUI />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Cards;
