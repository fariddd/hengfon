import React, { Component } from 'react'

import '../index.css'

export default class Footer extends Component {
  render() {
    return (
      <div class="footer-copyright">
        <div class="copyrightSection">
          <span>Copyright &#169; 2020 Hengfon</span>
        </div>
      </div>
    );
  }
}
