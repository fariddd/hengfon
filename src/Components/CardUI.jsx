import React from 'react';
import hp1 from '../Assets/hp1(23).jpg';

const Card = (props) => {
  return(
    <div className="card text-center">
      <div className="overflow">
        <img src={hp1} alt="hengfon 1" className="d-flex justify-content-center"/>
      </div>
      <div className="card-body text-dark">
        <h4 className="card-title">Card title</h4>
        <p className="card-text text-secondary">Lorem ipsum dolor sit amet</p>
        <a href="#" className="btn btn-outline-success">Tambah ke keranjang</a>
      </div>
    </div>
  );
}

export default Card;